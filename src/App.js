/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useState, useEffect, useCallback} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  BackHandler,
  Platform,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  Avatar,
  TouchableRipple,
  Appbar,
  Drawer,
  Divider,
  withTheme,
  IconButton,
  Colors,
  Surface,
  Caption,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Routes from './Routes';
import {withTitle} from '@components/Title';
import ErrorBoundary from '@components/ErrorBoundary';
import AppSettings from '@modules/AppSettings';
import URL from 'url-parse';
import GitLabAPI from './GitLabAPI.js';

const App = props => {
  const colors = props.theme.colors;
  const {history} = props;
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [accounts, setAccounts] = useState([]);
  const [currentAccount, setCurrentAccount] = useState(null);
  const [viewingAccounts, setViewingAccounts] = useState(false);
  const [user, setUser] = useState(null);
  const [deviceWidth, setDeviceWidth] = useState(
    Dimensions.get('window').width,
  );

  useEffect(() => {
    AppSettings.users().then(setAccounts);
    AppSettings.currentUserId().then(setCurrentAccount);
    const gitlab = new GitLabAPI();
    gitlab.user().then(setUser);
  }, [viewingAccounts, drawerOpen]);

  const orientationChangeHandler = useCallback(({screen, window}) => {
    setDeviceWidth(window.width);
  }, []);

  useEffect(() => {
    Dimensions.addEventListener('change', orientationChangeHandler);
    return () => {
      Dimensions.removeEventListener('change', orientationChangeHandler);
    };
  }, [orientationChangeHandler]);

  const drawType = deviceWidth > 700 ? 'permanent' : 'temporary';
  let drawStyles = {width: 240};
  if (drawType == 'temporary') {
    drawStyles = {
      ...drawStyles,
      position: 'absolute',
      elevation: 1,
      zIndex: 999,
      bottom: 0,
      top: 0,
      left: 0,
    };
    if (Platform.OS === 'android') {
      drawStyles.paddingTop = 60;
    }
  }
  const styles = StyleSheet.create({
    drawer: drawStyles,
    shadow: {
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      zIndex: 990,
      backgroundColor: 'rgba(0,0,0,0.6)',
    },
  });

  const navigateToPage = page => {
    history.push(page);
    setDrawerOpen(false);
  };

  let drawerSections = [
    {
      title: 'Work',
      items: [
        {title: 'Todos', page: '/', icon: 'check-all'},
        {
          title: 'Merge Requests',
          page: '/merge-requests',
          icon: 'source-merge',
        },
        {title: 'Projects', page: '/projects', icon: 'book-multiple'},
      ],
      flex: 2,
    },
    {
      items: [{title: 'Settings', page: '/settings', icon: 'settings'}],
      flex: 1,
    },
  ];
  if (viewingAccounts) {
    const items = [
      ...accounts.map((item, index) => ({
        title: new URL(item.host).host,
        page: `/accounts/select/${index}`,
        icon: null,
      })),
      {title: 'New Account', page: '/accounts/new', icon: 'account-plus'},
    ];
    drawerSections = [
      {
        title: 'Accounts',
        items,
      },
    ];
  }

  let appBarIcon = <></>;
  const isHomePage = history.location.pathname.split('/').length == 2;
  if (isHomePage && drawType == 'temporary') {
    if (drawerOpen) {
      appBarIcon = (
        <Appbar.Action
          icon="close"
          onPress={() => {
            setDrawerOpen(false);
          }}
        />
      );
    } else {
      appBarIcon = (
        <Appbar.Action
          icon="menu"
          onPress={() => {
            setViewingAccounts(false);
            setDrawerOpen(true);
          }}
        />
      );
    }
  } else if (!isHomePage && history.canGo(-1)) {
    if (
      history.location.pathname !== '/accounts/new' &&
      accounts[currentAccount] !== undefined
    ) {
      appBarIcon = (
        <Appbar.Action icon="arrow-left" onPress={() => history.goBack()} />
      );
    }
  }

  const backHandler = useCallback(() => {
    if (
      !isHomePage &&
      (history.location.pathname !== '/accounts/new' &&
        accounts[currentAccount] !== undefined)
    ) {
      history.goBack();
      return true;
    }
    return false;
  }, [accounts, currentAccount, history, isHomePage]);

  // Putting the if outside of the useEffect causes an error in eslint
  useEffect(() => {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', backHandler);
    }
    return () => {
      if (Platform.OS === 'android') {
        BackHandler.removeEventListener('hardwareBackPress', backHandler);
      }
    };
  }, [backHandler]);

  let account = accounts[currentAccount];
  if (accounts[currentAccount] === undefined) {
    account = {host: 'https://gitlab.com/', accessToken: ''};
  }

  return (
    <View style={{flex: 1, flexAlign: 'center', backgroundColor: 'white'}}>
      <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
      <View style={{flex: 1, flexDirection: 'row'}}>
        {(drawerOpen || drawType == 'permanent') && (
          <>
            <Surface style={[styles.drawer, {backgroundColor: colors.surface}]}>
              <TouchableRipple
                onPress={() => setViewingAccounts(!viewingAccounts)}>
                <View
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    padding: 15,
                  }}>
                  {user === null && <Avatar.Icon icon="account" size={44} />}
                  {user !== null && (
                    <Avatar.Image source={{uri: user.avatar_url}} size={44} />
                  )}
                  <Caption style={{marginLeft: 10, flex: 2}}>
                    {new URL(account.host).host}
                  </Caption>
                  <Icon
                    name={viewingAccounts ? 'chevron-up' : 'chevron-down'}
                    size={20}
                  />
                </View>
              </TouchableRipple>
              <Divider />
              <View style={{flexDirection: 'column'}}>
                {drawerSections.map((section, i) => (
                  <Drawer.Section title={section.title} key={i}>
                    {section.items.map((item, index) => (
                      <Drawer.Item
                        key={index}
                        label={item.title}
                        icon={item.icon}
                        onPress={() => navigateToPage(item.page)}
                        active={history.location.pathname == item.page}
                      />
                    ))}
                  </Drawer.Section>
                ))}
              </View>
            </Surface>
            {drawType == 'temporary' && (
              <TouchableWithoutFeedback onPress={() => setDrawerOpen(false)}>
                <View style={styles.shadow} />
              </TouchableWithoutFeedback>
            )}
          </>
        )}
        <View style={{flex: 1, flexDirection: 'column'}}>
          <Appbar.Header>
            {appBarIcon}
            <Appbar.Content title={props.title} />
          </Appbar.Header>
          <View
            style={{
              flex: 1,
              flexGrow: 1,
              backgroundColor: '#FAFAFA',
              paddingTop: 10,
            }}>
            <ErrorBoundary>
              <Routes />
            </ErrorBoundary>
          </View>
        </View>
      </View>
    </View>
  );
};

export default withTheme(withRouter(withTitle(App)));
