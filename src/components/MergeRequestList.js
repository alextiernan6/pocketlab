import React, {useState} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {
  List,
  Caption,
  ActivityIndicator,
  Colors,
  Divider,
  IconButton,
  Portal,
  Dialog,
  Paragraph,
  Title,
  Button,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import moment from 'moment';
import Illustration from '@gitlab/svgs/dist/illustrations/merge_request_changes_empty.svg';
//import Illustration from '@gitlab/svgs/dist/illustrations/todos_all_done.svg';

const filters = ['Opened', 'Merged', 'Closed', 'All'];

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
    alignItems: 'center',
  },
  emptyListContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const MergeRequestList = props => {
  const {mergeRequests, currentFilter, onCurrentFilterChanged} = props;
  const [showFilter, setShowFilter] = useState(false);
  const [internalFilter, setInternalFilter] = useState(currentFilter);

  if (mergeRequests === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <>
      {currentFilter !== undefined && (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            paddingRight: 10,
          }}>
          <IconButton
            icon="tune"
            size={20}
            onPress={() => setShowFilter(true)}
          />
        </View>
      )}
      <FlatList
        data={mergeRequests}
        ItemSeparatorComponent={Divider}
        keyExtractor={item => item.id}
        contentContainerStyle={
          mergeRequests.length === 0 ? styles.listContainer : {}
        }
        ListEmptyComponent={() => (
          <View style={styles.emptyListContainer}>
            <Illustration width="90%" height="80%" />
            <Title>There are no {currentFilter} merge requests</Title>
          </View>
        )}
        renderItem={({item, index}) => {
          return (
            <List.Item
              onPress={() =>
                props.history.push(
                  `/projects/${item.project_id}/merge_request/${
                    item.iid
                  }/overview`,
                )
              }
              title={item.title}
              description={moment(item.updated_at).fromNow()}
            />
          );
        }}
      />

      {currentFilter !== undefined && (
        <Portal>
          <Dialog
            visible={showFilter}
            onDismiss={() => {
              setInternalFilter(currentFilter);
              setShowFilter(false);
            }}>
            <Dialog.Title>Filter</Dialog.Title>
            <FlatList
              data={filters}
              ItemSeparatorComponent={Divider}
              keyExtractor={item => item}
              renderItem={({item}) => (
                <List.Item
                  title={item}
                  left={leftProps => (
                    <List.Icon
                      {...leftProps}
                      color="primary"
                      icon={
                        internalFilter == item.toLowerCase()
                          ? 'radiobox-marked'
                          : 'radiobox-blank'
                      }
                    />
                  )}
                  onPress={() => setInternalFilter(item.toLowerCase())}
                />
              )}
            />
            <Dialog.Actions>
              <Button onPress={() => setShowFilter(false)}>Cancel</Button>
              <Button
                onPress={() => {
                  setShowFilter(false);
                  onCurrentFilterChanged(internalFilter);
                }}>
                Ok
              </Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      )}
    </>
  );
};

export default withRouter(MergeRequestList);
