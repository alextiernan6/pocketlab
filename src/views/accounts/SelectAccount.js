import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {withTheme, TextInput, Card, Button, FAB} from 'react-native-paper';
import {useHistory, useRouteMatch} from 'react-router-native';
import {withTitle} from '@components/Title';
import AppSettings from '@modules/AppSettings';
import GitLabAPI from '../../GitLabAPI';

const SelectAccount = props => {
  const history = useHistory();
  const match = useRouteMatch('/accounts/select/:id');

  useEffect(() => {
    AppSettings.setCurrentUserId(match.params.id).then(user => {
      const gitlab = new GitLabAPI();
      gitlab.setConnection(user.host, user.accessToken);
      history.goBack();
    });
  }, [history, match.params.id]);
  return <></>;
};

export default SelectAccount;
