import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../../GitLabAPI';
import MergeRequestList from '@components/MergeRequestList';

const ListMergeRequests = props => {
  const {project} = props.match.params;
  const [mergeRequests, setMergeRequests] = useState(null);
  const [filter, setFilter] = useState('opened');

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.projectMergeRequests(project, {state: filter}).then(response => {
      if (!Array.isArray(response)) {
        response = [];
      }
      setMergeRequests(response);
    });
  }, [project, filter]);

  return (
    <MergeRequestList
      currentFilter={filter}
      onCurrentFilterChanged={setFilter}
      mergeRequests={mergeRequests}
    />
  );
};

export default withRouter(ListMergeRequests);
