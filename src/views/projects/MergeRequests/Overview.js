import React, {useEffect, useState, useCallback} from 'react';
import {Paragraph, Card, Button, ActivityIndicator} from 'react-native-paper';
import {withRouter, Link} from 'react-router-native';
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import Markdown from 'react-native-markdown-renderer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import GitLabAPI from '../../../GitLabAPI';
import Changes from './Changes';
import Discussion from '@components/Discussion';
import {withTitle} from '@components/Title';
import statusMap from '../Pipelines/StatusMap';

const styles = StyleSheet.create({
  branchName: {
    fontWeight: 'bold',
  },
});

const ViewMergeRequest = props => {
  const {project, merge_request, tab} = props.match.params;
  const [mergeRequest, setMergeRequest] = useState(null);

  const loadMergeRequest = useCallback(() => {
    const gitlab = new GitLabAPI();
    gitlab.mergeRequest(project, merge_request).then(response => {
      setMergeRequest(response);
    });
  }, [merge_request, project]);

  useEffect(loadMergeRequest, [merge_request, project]);

  const merge = async () => {
    const gitlab = new GitLabAPI();
    gitlab.acceptMergeRequest(project, merge_request).then(() => {
      setMergeRequest(null);
      loadMergeRequest();
    });
  };

  if (mergeRequest === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <ScrollView style={{padding: 10, paddingTop: 0}}>
      <Markdown>{mergeRequest.description}</Markdown>
      <Card>
        <Card.Content>
          <Paragraph>
            Request to merge{' '}
            <Paragraph style={styles.branchName}>
              {mergeRequest.source_branch}
            </Paragraph>{' '}
            into{' '}
            <Paragraph style={styles.branchName}>
              {mergeRequest.target_branch}
            </Paragraph>
          </Paragraph>
        </Card.Content>
      </Card>
      {mergeRequest.head_pipeline !== null && (
        <Card style={{marginTop: 10}}>
          <Card.Content style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              style={{marginRight: 10}}
              color={statusMap[mergeRequest.head_pipeline.status].color}
              size={30}
              name={statusMap[mergeRequest.head_pipeline.status].icon}
            />
            <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
              <Paragraph>
                {mergeRequest.head_pipeline.before_sha ===
                  '0000000000000000000000000000000000000000' && (
                  <Paragraph>Detached merge request </Paragraph>
                )}
                pipeline{' '}
              </Paragraph>
              <Link to="/projects/15529274/pipelines/110475858">
                <Paragraph style={styles.branchName}>
                  #{mergeRequest.head_pipeline.id}{' '}
                </Paragraph>
              </Link>
              <Paragraph>
                {mergeRequest.head_pipeline.detailed_status.text} for{' '}
                <Paragraph style={styles.branchName}>
                  {mergeRequest.head_pipeline.sha.slice(0, 8)}
                </Paragraph>
              </Paragraph>
            </View>
          </Card.Content>
        </Card>
      )}
      <Card style={{marginTop: 10}}>
        <Card.Content style={{flexDirection: 'row', alignItems: 'center'}}>
          {mergeRequest.merged_by !== null && (
            <Paragraph>
              Merged by {mergeRequest.merged_by.name}{' '}
              {moment(mergeRequest.merged_at).fromNow()}
            </Paragraph>
          )}
          {mergeRequest.merged_by === null && (
            <View>
              <Button
                disabled={mergeRequest.user.can_merge === false}
                onPress={merge}
                mode="contained">
                Merge
              </Button>
              {mergeRequest.user.can_merge === false && (
                <Paragraph>
                  Ready to be merged automatically. Ask someone with write
                  access to this repository to merge this request
                </Paragraph>
              )}
            </View>
          )}
        </Card.Content>
      </Card>
    </ScrollView>
  );
};

export default withTitle(withRouter(ViewMergeRequest));
