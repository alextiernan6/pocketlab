import React, {useState} from 'react';
import {
  StyleSheet,
  Image,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  Card,
  TextInput,
  Button,
  Switch,
  Paragraph,
  Divider,
  List,
  withTheme,
  Modal,
  Portal,
  Chip,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import Markdown from 'react-native-markdown-renderer';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import GitLabAPI from '../../../GitLabAPI';
import SelectorModal from './SelectorModal';
import moment from 'moment';

const styles = StyleSheet.create({
  description: {
    marginTop: 10,
  },
  saveButton: {
    marginLeft: 9,
  },
  assigneeContainer: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 10,
  },
});

const EditIssue = props => {
  const {params} = props.match;
  const isNewIssue = params.iid == undefined;
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [isConfidential, setIsConfidential] = useState(false);
  const [dueDate, setDueDate] = useState('');
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [assignees, setAssignees] = useState([]);
  const [showAssigneesModal, setShowAssigneesModal] = useState(false);
  const [milestone, setMilestone] = useState(null);
  const [showMilestoneModal, setShowMilestoneModal] = useState(false);

  const saveIssue = () => {
    const gitlab = new GitLabAPI();
    const body = {
      title,
      description,
      confidential: isConfidential,
      due_date: dueDate,
      milestone_id: milestone != null ? milestone.id : '',
      assignee_ids: assignees.map(user => user.id),
    };
    if (isNewIssue) {
      gitlab.newIssue(params.project, body).then(response => {
        console.log(response);
        if (response.iid) {
          props.history.push(
            `/projects/${params.project}/issues/${response.iid}`,
          );
        }
      });
    }
  };

  return (
    <ScrollView keyboardDismissMode="on-drag">
      <Card style={{margin: 10}}>
        <Card.Content>
          <TextInput
            mode="outlined"
            value={title}
            onChangeText={setTitle}
            label="Title"
          />
          <TextInput
            style={styles.description}
            mode="outlined"
            value={description}
            onChangeText={setDescription}
            label="Description"
            multiline
          />
          <View>
            <Paragraph>
              This issue is confidential and should only be visible to team
              members with at least Reporter access.
            </Paragraph>
            <Switch
              value={isConfidential}
              onValueChange={() => setIsConfidential(!isConfidential)}
              color={props.theme.colors.primary}
            />
          </View>
          <Divider />
          <TouchableOpacity onPress={() => setShowDatePicker(true)}>
            <TextInput
              mode="outlined"
              value={dueDate == '' ? '' : moment(dueDate).format('L')}
              caretHidden={true}
              editable={false}
              pointerEvents="none"
              label="Due Date"
            />
          </TouchableOpacity>
          <DateTimePickerModal
            mode="date"
            display="default"
            isVisible={showDatePicker}
            value={dueDate}
            onCancel={() => {
              setShowDatePicker(false);
              setDueDate('');
            }}
            onConfirm={date => {
              setShowDatePicker(false);
              setDueDate(date);
            }}
          />
          <TouchableOpacity onPress={() => setShowMilestoneModal(true)}>
            <TextInput
              mode="outlined"
              value={milestone !== null ? milestone.title : ''}
              label="Milestone"
              editable={false}
              pointerEvents="none"
            />
          </TouchableOpacity>
          <SelectorModal
            project={params.project}
            scope="milestones"
            open={showMilestoneModal}
            onSelected={(id, title) => {
              setMilestone({id, title});
              setShowMilestoneModal(false);
            }}
            onClose={() => setShowMilestoneModal(false)}
          />
          <View style={styles.assigneeContainer}>
            {assignees.map(assignee => (
              <Chip>{assignee.title}</Chip>
            ))}
          </View>
          <Button onPress={() => setShowAssigneesModal(true)}>
            Add Assignee(s)
          </Button>
          <SelectorModal
            project={params.project}
            scope="users"
            open={showAssigneesModal}
            onSelected={(id, title) => {
              setAssignees([...assignees, {id, title}]);
            }}
            onClose={() => setShowAssigneesModal(false)}
          />
        </Card.Content>
        <Card.Actions>
          <Button
            disabled={title == ''}
            style={styles.saveButton}
            mode="contained"
            onPress={saveIssue}>
            {isNewIssue ? 'Submit Issue' : 'Save Issue'}
          </Button>
        </Card.Actions>
      </Card>
    </ScrollView>
  );
};

export default withRouter(withTheme(EditIssue));
