import React, {useState} from 'react';
import {StyleSheet, Image, ScrollView, View, Text} from 'react-native';
import {
  Card,
  Paragraph,
  Headline,
  Caption,
  ActivityIndicator,
  Avatar,
} from 'react-native-paper';
import Markdown from 'react-native-markdown-renderer';
import moment from 'moment';

const styles = StyleSheet.create({
  codeInline: {
    backgroundColor: '#fbe5e1',
    color: '#c0341d',
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 20,
    paddingRight: 2,
  },
});

const Description = props => {
  const {issue} = props;
  const when = moment(issue.created_at).fromNow();
  return (
    <ScrollView>
      <Card style={{margin: 10}}>
        <Card.Content>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Avatar.Image size={25} source={{uri: issue.author.avatar_url}} />
            <Text style={{flex: 1, marginLeft: 5, marginTop: 2}}>
              {issue.author.name}
            </Text>
            <Caption>{when}</Caption>
          </View>
          <Headline>
            #{issue.iid} · {issue.title}
          </Headline>
          <Markdown style={styles}>{issue.description}</Markdown>
        </Card.Content>
      </Card>
    </ScrollView>
  );
};

export default Description;
